# Anything To QR 

Generates a pdf to be printed representing the content of the given file. It
also recovers it! :)

# Dependencies

```
apt-get install poppler-utils texlive-fonts-recommended texlive-latex-base qrencode zbar-tools
```

# Usage


# Others:

Heavily based on https://github.com/nurupo/paper-store . My changes:
- [ ] Parse arguments in a good way
- [ ] handle different invokation (e.g., a2qr qr2a)
- [p] Add useful option (e.g., self-test for generation)
- [x] Printing more information (i.e., versions of things used)
- [x] Consolidate the 2 methods spare and dense into a single one that is a middle ground
- [x] **Added header to each blob sent to QR to automatically find the right reconstruction order**
- [x] Moved to python
- [x] Separated generation and reconstruction

Some other implementations I found
- https://gist.github.com/joostrijneveld/59ab61faa21910c8434c
