#!/usr/bin/env python3


import subprocess
import argparse
import sys
import tempfile
import os.path
import pprint
import shutil
import base64
import re
import hashlib


__VERSION__ = 0.1

def textReflow(qr_line, column_size=45):
  reflowed = []
  for start in range(0, len(qr_line), column_size):
    end = min(len(qr_line), start + column_size)
    reflowed.append(qr_line[start:end])
  return "\n".join(reflowed)


def escapeStringForLatex(s):
  return s.replace('_', '\\_').replace('%', '\\%')


def writeLatexFile(latex_fp, file_to_qr: dict, header, footer, paper_size="a4paper"):
  # TODO: use an OCR specific font to help in recovery
  latex_fp.write("""\\documentclass[10pt,%s]{article}
\\usepackage[left=1cm,right=1cm,top=2cm,bottom=2cm]{geometry}
\\usepackage[utf8]{inputenc}
\\usepackage{graphicx}
\\usepackage{fancyhdr}
\\usepackage{lastpage}

\\makeatletter
\\newcommand\\notsotiny{\\@setfontsize\\notsotiny{12}{7.1828}}
\\makeatother

\\renewcommand{\\headrulewidth}{0pt}

\\chead{\\texttt{%s}}
\\cfoot{\\texttt{%s -- Page \\thepage~of~\\pageref{LastPage}}}

\\begin{document}
\\pagestyle{fancy}
\\fontfamily{pcr}\\selectfont
\\noindent""" % (paper_size, header, footer))

  print_linebreak = False
  for filename in file_to_qr:
    if print_linebreak:
      latex_fp.write("\\\\")
    print_linebreak = True
    latex_fp.write("""\\begin{minipage}{0.45\\linewidth}%%
\\includegraphics[width=\\linewidth]{%s}%%
\\end{minipage}\\begin{minipage}{0.53\\linewidth}%%
\\notsotiny{\\begin{verbatim}%s\\end{verbatim}}%%
\\end{minipage}"""
        % (filename, textReflow(file_to_qr[filename])))

  latex_fp.write("\\end{document}")



def encode(filename, output_filename=None, keep_work=False):
  # Number of lines in the base64 encoding that is used in each qr code. It does NOT
  # take the overhead of the marks (QRXX) into consideration
  chars_per_qr = 760
  tmp_dir = tempfile.mkdtemp(prefix='a2qr.tmp.')

  print("filename =", filename)
  print("tmp_dir =", tmp_dir)

  # TODO: do this in python
  with open(filename, 'rb') as f:
    data = f.read()

  data_b64 = base64.b64encode(data).decode('utf-8')
  assert len(data_b64) > 0

  qr_idx = 0
  file_to_qr = {}

  for start in range(0,len(data_b64),chars_per_qr):
    end = min(len(data_b64), start + chars_per_qr)
#     print(start, end)

    qr_filename = '%s/qr%02d.png' % (tmp_dir, qr_idx)
    qr_content = "QR%02d-%s" % (qr_idx, data_b64[start:end])

    # TODO: do this in python
    subprocess.check_output(['qrencode', '-o', qr_filename, qr_content])
    assert os.path.isfile(qr_filename)

    file_to_qr[qr_filename] = qr_content

    qr_idx += 1
    assert qr_idx < 100

#   pprint.pprint(file_to_qr)

  latex_filename = '%s/all_qr_codes.tex' % tmp_dir
  md5sum = hashlib.md5(open(filename,'rb').read()).hexdigest()
  header = "%s -- md5sum: %s" % (escapeStringForLatex(os.path.basename(filename)), md5sum)

  zbarimg_version = subprocess.check_output(['zbarimg', '--version']).decode('utf-8')
  qrencode_version = subprocess.check_output(['qrencode', '--version'],
                                             stderr=subprocess.STDOUT).decode('utf-8')
  qrencode_version = qrencode_version.replace('\n', ' ').split(' ')[2]
  footer = 'a2qr %s, qrencode %s, zbarimg %s' % (__VERSION__, qrencode_version, zbarimg_version)
  with open(latex_filename, 'w') as f:
    writeLatexFile(f, file_to_qr, header=header, footer=footer)

  # Running pdflatex twice to get the total page number
  pdflatex_cmd = ['pdflatex', '-interaction', 'nonstopmode', '-output-directory', tmp_dir,
                      latex_filename]
  subprocess.check_output(pdflatex_cmd)
  subprocess.check_output(pdflatex_cmd)


  generated_pdf = latex_filename[0:-3] + 'pdf'
  sys.stdout.write("Testing generated pdf (%s)...  " % generated_pdf)
  reconstructed_file = "%s/reconstructed" % tmp_dir
  decode(generated_pdf, output_filename=reconstructed_file)

  # TODO: compare byte-wise
  reconstr_md5sum = hashlib.md5(open(reconstructed_file,'rb').read()).hexdigest()

  if md5sum != reconstr_md5sum:
    print('ERROR! Keeping intermediary files at', tmp_dir)
    sys.exit(1)

  print('passed!')
  if output_filename is None:
    output_filename = filename + '.pdf'

  print('Output:', output_filename)
  shutil.move(generated_pdf, output_filename)

  if keep_work:
    print('Keeping intermediary files at', tmp_dir)
  else:
    shutil.rmtree(tmp_dir)




def decode(pdf_filename, output_filename=None):
  all_parsed_qr = subprocess.check_output(['zbarimg', '--raw', '-Sdisable', '-Sqrcode.enable',
                                              pdf_filename]).decode("utf-8")
  all_parsed_qr = all_parsed_qr.strip()

#   pprint.pprint(all_parsed_qr)

  block_to_data = {}
  for line in all_parsed_qr.split('\n'):
#     print(line, len(line))
    assert re.match("QR[0-9][0-9]-", line)
    b = line[0:4]
    block_to_data[b] = line[5:]

#   pprint.pprint(block_to_data)
  ordered_lines = []
  for b in sorted(block_to_data):
    ordered_lines += block_to_data[b]

  if output_filename is None:
    output_filename = pdf_filename[0:-4] + '.data'
    print("Output:", output_filename)

  with open(output_filename, 'wb') as f:
    f.write(base64.decodebytes(bytes("\n".join(ordered_lines), "utf-8")))




if __name__ == "__main__":
  if sys.argv[1] == 'decode':
    decode(sys.argv[2], output_filename=sys.argv[3])
  else:
    encode(sys.argv[2], output_filename=sys.argv[3], keep_work=False)
