#!/bin/bash

rm -rf test.git_ignore_me
mkdir test.git_ignore_me
cd test.git_ignore_me
size=$((1+(RANDOM%30)))
echo "Using ${size}K"
dd if=/dev/urandom of=data.bin bs=${size}K count=1
../a2qr.py encode data.bin data.pdf
../a2qr.py decode data.pdf decoded.bin
md5sum data.bin
md5sum decoded.bin
if ! diff -u data.bin decoded.bin; then
    echo "Original and parsed base64 are different!"
    exit 1
else
  echo "SUCCESS!"
fi
cd ..
